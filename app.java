import java.util.logging.*;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class app {

    public static void main(String[] args) {  

        Logger logger = Logger.getLogger("MyLog");  
        FileHandler fh;
        String EXTERNAL_LOG_DIR = System.getenv("EXTERNAL_LOG_DIR");
        Integer ERROR_LOG_INTERVAL = Integer.valueOf(System.getenv("ERROR_LOG_INTERVAL"));
        Integer INFO_LOG_INTERVAL = Integer.valueOf(System.getenv("INFO_LOG_INTERVAL"));

        try {  

            // This block configure the logger with handler and formatter  
            fh = new FileHandler(EXTERNAL_LOG_DIR + "/java.log");  
            // fh = new FileHandler("java.log");  
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();  
            fh.setFormatter(formatter);  

            Timer timer = new Timer();
            timer.schedule(new LogInfo(logger), 0 , ERROR_LOG_INTERVAL * 1000);
            timer.schedule(new LogWarning(logger), 0 ,  INFO_LOG_INTERVAL * 1000);

        } catch (SecurityException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }
    }
}

class LogInfo extends TimerTask  {

    Logger logger;
    public LogInfo (Logger logger) { this.logger = logger;}

    public void run() {
        this.logger.log(Level.INFO, "Java Informational Message");
    }
}

class LogWarning extends TimerTask  {

    Logger logger;
    public LogWarning (Logger logger) { this.logger = logger;}

    public void run() {
        this.logger.log(Level.WARNING, "Java Error Info");
    }
}
