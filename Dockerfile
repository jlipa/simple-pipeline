FROM openjdk:11-jdk-slim

ADD ./app.java /app.java
ADD ./java-entry.sh /entrypoint.sh

CMD mkdir /java-logs

ENTRYPOINT [ "/entrypoint.sh" ]